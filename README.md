# Food Extrudability Assessment and Prediction

This is a set of accompanied codes for food extrudability assessment and prediction from a  study from the Food Process Engineering Group at Wageningen University. For more information, please refer to https://www.sciencedirect.com/science/article/pii/S146685642100165X. 

## Extrudability Assessment 

Example pictures of line extursion can be found in the `Sample Image` folder. You can run the `Preprocessing.R` script to correct rotations and calculate pixel distance. See annotations in script for preprocessing your own images. Once the image is preprocessed. You can run the `Get_measurement.R` function. You can also loop the function for batch processing of images taken from one run. 

## Extrudability Prediction

You can run the script `Extrudability_prediction` to reproduce results from our paper. A few data cleaning and feature generation steps were put before the actual modelling step. The extrudability measurements are saved as `Extrudability_total.csv`. 


| Columns | Discription |
| ------ | ------ |
| Type | Product type |
| Temp | Printing temperature |
| Nozzle | Nozzle size |
| Pressure | Printing pressure |
| Speed | Nozzle speed |
| n | Flow index |
| K | Consistency coefficient  |
| m | Flow rate coefficient  |
| b | Flow rate exponent |
| mean_wd | Averaged mode width |
| mean_con | Averaged width consistency |
| mean_ln | Averaged line width |
| Pic | Image file index |

## Citation

Ma, Y., Schutyser, M. A., Boom, R. M., & Zhang, L. (2021). Predicting the extrudability of complex food materials during 3D printing based on image analysis and gray-box data-driven modelling. Innovative Food Science & Emerging Technologies, 102764.

